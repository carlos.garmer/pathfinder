import processing.serial.*;

//Escoga el número de cuadros a lo largo y a lo ancho

int largo=10, ancho=6;

////////////////////////////////////////////////////////
int l=0 , a=0;
int colorR, colorG, colorB, i=1;
int xI=0, yI=0;
int xF=largo-1, yF=ancho-1;
int xB=largo-1, yB=0;
int xE, yE;
boolean determinado=false,shortpath=false, dis0=true,dis1=true, dis2=true,dis3=true;
int [] rectX = new int [largo];
int [] rectY = new int [ancho];
int [] blockX = new int [largo*ancho];
int [] blockY = new int [largo*ancho];
int [] recorridoX = new int [largo*ancho];
int [] recorridoY = new int [largo*ancho];
float [] distancia = new float [4];
float disMEN;
int veces=0, u=0 , b=0;
float tancho, tlargo;
void setup () {
  fill(0,0,0);
  size (1600, 1000);
}

void determinarT(){
  if (determinado==false){
    tancho=((900-20*ancho))/ancho;
    tlargo=((1600-20*largo))/largo;
    for (int filas=0;filas<1590-20*largo; filas+=tlargo){
      rectX[l]=filas+10;
      l++;
    }
    for (int columnas=0; columnas<890-20*ancho; columnas+=tancho){
      rectY[a]=columnas+10;
      a++;
    }
    determinado=true;
  }
}

void imprimirCuadricula() {
    for (int filas=10; filas<1600-20*largo; filas+=tlargo) {
      for (int columnas=10; columnas<900-20*ancho; columnas+=tancho) {
        fill(colorR, colorG, colorB);
        strokeWeight(10);
        rect(filas+10, columnas+10, tlargo-10,tancho-10);
    }
  }
}

void posInicio() {
  if (xI>=l) xI=l-1;
  if (xI<0) xI=0;
  if (yI>=a) yI=a-1;
  if (yI<0) yI=0;
  fill(colorR, colorG, colorB);
  noStroke();
  if(xI<l+1&&xI>-1&&yI<a&yI>-1){
    println("X I " + xI);
    println("Y I " + yI);
    ellipse(rectX[xI]+(tlargo)/2, rectY[yI]+(tancho)/2, 50, 50);
  }
}

void posFinal() {
  if (xF>=l) xF=l-1;
  if (xF<0) xF=0;
  if (yF>=a) yF=a-1;
  if (yF<0) yF=0;
  fill(colorR, colorG, colorB);
  noStroke();
  if(xF<l&&xF>-1&&yF<a&&yF>-1){
    println("X  f " + xF);
    println("Y  f " + yF);
    ellipse(rectX[xF]+(tlargo)/2, rectY[yF]+(tancho)/2, 50, 50);
  }
}

void posBlock() {
  if (xB>=l) xB=l-1;
  if (xB<0) xB=0;
  if (yB>=a) yB=a-1;
  if (yB<0) yB=0;
  fill(colorR, colorG, colorB);
  noStroke();
  if(xB<l&&xB>-1&&yB<a&&yB>-1){
    println("X  B " + xF);
    println("Y  B " + yF);
    rect(rectX[xB]+10, rectY[yB]+10, tlargo-10, tancho-10);
  }
}
void keyPressed(){
  if (veces==0){
    switch(keyCode) {
    case UP :
      yI--;
      println("Revisando");
      break;
    case DOWN:
      yI++;
      break;
    case RIGHT:
      xI++;
      break;
    case LEFT:
      xI--;
      break;
    }
    if (key=='w') yF--;
    if (key=='s') yF++;
    if (key=='a') xF--;
    if (key=='d') xF++;
    if (key=='i') yB--;
    if (key=='k') yB++;
    if (key=='j') xB--;
    if (key=='l') xB++;
    if (key=='.') listaBlock();
    if (key==ENTER) shortpath=true;
  }
}

void listaBlock(){
  blockX[u]=rectX[xB];
  blockY[u]=rectY[yB];
  u++;
}

void imprimirBlock(){
  for(int i=0;i<u;i++){
    fill(colorR, colorG, colorB);
    rect(blockX[i]+10, blockY[i]+10, tlargo-10, tancho-10);
  }
}


void considerar(){
  for(int i=0; i<u;i++){
  if (yE+1<=a-1){
  if (rectX[xE]==blockX[i]&&rectY[yE+1]==blockY[i]){ 
  dis0=false;
  distancia[0]=10000;
  }}
  else{
 if (rectX[xE]==blockX[i]&&rectY[yE]==blockY[i]){ 
  dis0=false;
  distancia[0]=10000;  }
  }
  if (yE-1>=0){
  if (rectX[xE]==blockX[i]&&rectY[yE-1]==blockY[i]){
  dis1=false;
  distancia[1]=10000;
  }}
  else{
  if (rectX[xE]==blockX[i]&&rectY[yE]==blockY[i]){
  dis1=false;
  distancia[1]=10000;
  }
  }
  if (xE+1<=l-1){
  if (rectX[xE+1]==blockX[i]&&rectY[yE]==blockY[i]) {
  dis2=false;
  distancia[2]=10000;
  }}
  else{
   if (rectX[xE]==blockX[i]&&rectY[yE]==blockY[i]) {
  dis2=false;
  distancia[2]=10000;
  }   
  }
  if (xE-1>=0){
  if (rectX[xE-1]==blockX[i]&&rectY[yE]==blockY[i]) {
  dis3=false;
  distancia[3]= 10000;
  }}
  else{
    if (rectX[xE]==blockX[i]&&rectY[yE]==blockY[i]) {
  dis3=false;
  distancia[3]= 10000;
  } 
  }
  }
  

for(int i=0; i<b;i++){
  if (yE+1<=a-1){
  if (rectX[xE]==recorridoX[i]&&rectY[yE+1]==recorridoY[i]){ 
  dis0=false;
  distancia[0]=10000;
  }}
  else{
 if (rectX[xE]==recorridoX[i]&&rectY[yE]==recorridoY[i]){ 
  dis0=false;
  distancia[0]=10000;  }
  }
  if (yE-1>=0){
  if (rectX[xE]==recorridoX[i]&&rectY[yE-1]==recorridoY[i]){
  dis1=false;
  distancia[1]=10000;
  }}
  else{
  if (rectX[xE]==recorridoX[i]&&rectY[yE]==recorridoY[i]){
  dis1=false;
  distancia[1]=10000;
  }
  }
  if (xE+1<=l-1){
  if (rectX[xE+1]==recorridoX[i]&&rectY[yE]==recorridoY[i]) {
  dis2=false;
  distancia[2]=10000;
  }}
  else{
   if (rectX[xE]==recorridoX[i]&&rectY[yE]==recorridoY[i]) {
  dis2=false;
  distancia[2]=10000;
  }   
  }
  if (xE-1>=0){
  if (rectX[xE-1]==recorridoX[i]&&rectY[yE]==recorridoY[i]) {
  dis3=false;
  distancia[3]= 10000;
  }}
  else{
    if (rectX[xE]==recorridoX[i]&&rectY[yE]==recorridoY[i]) {
  dis3=false;
  distancia[3]= 10000;
  } 
  }
  }  
}
  
void comparar(){
  if (veces==0){
  xE=xI;
  yE=yI;
  veces++;
  colorR=0; colorG=255; colorB=46;
  fill(colorR, colorG, colorB,50);
  if( xE>0&&yE>0)
  rect(rectX[xE-1], rectY[yE], tlargo-10, tancho-10);
  }
  
  
  //Colorear colores evaluados
  colorR=0; colorG=255; colorB=46;
  fill(colorR, colorG, colorB, 50);
  noStroke();
  if (yE+1<a) rect(rectX[xE]+10, rectY[yE+1]+ 10, tlargo-20, tancho-10);
  if (yE-1>0) rect(rectX[xE]+10, rectY[yE-1] +10, tlargo-20, tancho-10);
  if (xE+1<l) rect(rectX[xE+1]+10, rectY[yE]+10, tlargo-20, tancho-10);
  if (xE-1>0) rect(rectX[xE-1] +10, rectY[yE]+10, tlargo-20, tancho-10);
  
  if (veces!=0){
  colorR=255; colorG=217; colorB=5;
  fill(colorR, colorG, colorB);
  noStroke();
  if (yE>=a) yE=a;
  if (yE<=0) yE=0;
  if (xE>=l) yE=l;
  if (xE<=0) xE=0;
  rect(rectX[xE]+10, rectY[yE]+10, tlargo-10, tancho-10);
  recorridoX[b]=rectX[xE];
  recorridoY[b]=rectY[yE];
  b++;
  }
  
  
  if(yE+1<a) {distancia[0]= sqrt((rectX[xF]-(rectX[xE]))*(rectX[xF]-(rectX[xE]))+(rectY[yF]-rectY[(yE+1)])*(rectY[yF]-rectY[(yE+1)]))+2; //CUADRO ABAJO 
                } else distancia[0]= 100000;
  if(yE-1>-1) {distancia[1]= sqrt((rectX[xF]-(rectX[xE]))*(rectX[xF]-(rectX[xE]))+(rectY[yF]-rectY[(yE-1)])*(rectY[yF]-rectY[(yE-1)])) +1; 
  }else distancia[1]= 10000; //CUADRO ARRIBA
  if(xE+1<l) {distancia[2]= sqrt((rectX[xF]-(rectX[xE+1]))*(rectX[xF]-(rectX[xE+1]))+(rectY[yF]-rectY[(yE)])*(rectY[yF]-rectY[(yE)]))+3;//CUADRO DERECHA
  }else distancia[2]= 10000;
  if(xE-1>-1) {distancia[3]= sqrt((rectX[xF]-(rectX[xE-1]))*(rectX[xF]-(rectX[xE-1]))+(rectY[yF]-rectY[(yE)])*(rectY[yF]-rectY[(yE)]))+4 ; //CUADRO IZQUIERDA
  }else distancia[3]= 10000; 
  
  considerar();
  
  println("Distancia 0"+ distancia[0]);
  println("Distancia 1"+ distancia[1]);
  println("Distancia 2"+ distancia[2]);
  println("Distancia 3"+ distancia[3]);
  
  
  if (distancia[0]<distancia[1]&&distancia[0]<distancia[2]&&distancia[0]<distancia[3]&&dis0==true){
    disMEN=distancia[0];
    yE=yE+1;
    println("ABAJO");
  }
  if (distancia[1]<distancia[0]&&distancia[1]<distancia[2]&&distancia[1]<distancia[3]&dis1==true){
    disMEN=distancia[1];
    yE=yE-1;
    println("ARRIBA");
  }
  if (distancia[2]<distancia[0]&&distancia[2]<distancia[3]&&distancia[2]<distancia[3]&dis2==true){
    disMEN=distancia[2];
    xE=xE+1;
    println("DERECHA");
  }
    if (distancia[3]<distancia[0]&&distancia[3]<distancia[1]&&distancia[3]<distancia[2]&dis3==true){
    disMEN=distancia[3];
    xE=xE-1;
    println("IZQUIERDA");
  }
  
  colorR=0; colorG=255; colorB=243;
  posInicio();
  colorR=255; colorG=4; colorB=4;
  posFinal();
  
  if(disMEN<4){
    shortpath=false;
  }
  
  dis0=true;  dis1=true;   dis2=true;  dis3=true;
  
  println(dis0);
  println(dis1);
  println(dis2);
  println(dis3);
  println(xE);
  println(yE);
  
}




void draw () {
  
  if (shortpath==false&&veces==0){
  determinarT();
  colorR=255; colorG=255; colorB=255;
  imprimirCuadricula();
  colorR=0; colorG=255; colorB=243;
  posInicio();
  colorR=255; colorG=4; colorB=4;
  posFinal();
  colorR=0; colorG=0; colorB=0;
  imprimirBlock();
  colorR=158; colorG=145; colorB=142;
  posBlock();
  }
  
  if (shortpath==true){
    println("Encontrando camino más corto");
    comparar();
  }
  
  if(veces!=0){
  colorR=255; colorG=217; colorB=5;
  fill(colorR, colorG, colorB);
  noStroke();
  rect(rectX[xF]+10, rectY[yF]+10, tlargo-10, tancho-10);
  rect(rectX[xI]+10, rectY[yI]+10, tlargo-10, tancho-10);
  colorR=255; colorG=4; colorB=4;
  fill(colorR, colorG, colorB);
  noStroke();
  ellipse(rectX[xI]+(tlargo)/2, rectY[yI]+(tancho)/2, 50, 50);
  colorR=0; colorG=255; colorB=243;
  fill(colorR, colorG, colorB);
  noStroke();
  ellipse(rectX[xF]+(tlargo)/2, rectY[yF]+(tancho)/2, 50, 50);
  }
  //for(i=0;i<u;i++)
  //println("Posicion "+ i+"  "+ blockX[i]+","+blockY[i]);
}